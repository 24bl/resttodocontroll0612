package com.example.todo0612;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TodoServiceImpl implements TodoService {

  // private final TodoMapper todoMapper;

  // // TodoMapperをDIする
  // @Autowired
  // public TodoServiceImpl(TodoMapper todoMapper){
  //   this.todoMapper = todoMapper;
  // }

  // TodoMapperをDIする (上記と同じ意味)
  @Autowired
  private TodoMapper todoMapper;

  @Override
	public List<Todo> findAll(){
		List<Todo> todoList = todoMapper.findAll();
		return todoList;
	}
// ※type = class
  @Override
  public Todo findById(Integer id){
    Todo todo = todoMapper.findById(id);
    return todo;
  }

  @Override
  public void insert(Todo todo){
    todoMapper.insert(todo);
  }

  @Override
  public void update(Todo todo){
    todoMapper.update(todo);
  }

  @Override
  public void delete(Integer id){
    todoMapper.delete(id);
  }
}