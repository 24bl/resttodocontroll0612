package com.example.todo0612;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/todos")
public class RestTodoController2 {
  @Autowired
  private TodoRepository todoRepository;

  @GetMapping
  public List<Todo> getAllTodos() {
    List<Todo> todoList = todoRepository.findAll();

    Todo[] todoArray = new Todo[todoList.size()];
    int[] priorityNum = new int[todoList.size()];
    int[] drawArray = new int[3 + 1];

    for (int i = 0; i < todoArray.length; i++) {
      Todo todo = todoList.get(i);
      todoArray[i] = todo;

      if (todo.getPriority().equals("High")) {
        priorityNum[i] = 3;
      } else if (todo.getPriority().equals("Mid")) {
        priorityNum[i] = 2;
      } else {
        priorityNum[i] = 1;
      }
    }

    for (int i = 0; i < todoArray.length; i++) {
      int win = 0;
      int draw = 0;

      for (int m = 0; m < todoArray.length; m++) {
        if (i == m) {
          continue;
        }
        if (priorityNum[i] < priorityNum[m]) {
          win += 1;
        }
        if (priorityNum[i] == priorityNum[m]) {
          draw += 1;
        }
      }

      todoList.set(win + drawArray[priorityNum[i]], todoArray[i]);

      if (draw > 0) {
        drawArray[priorityNum[i]] += 1;
      }
    }
    return todoList;
  }

  @PostMapping
  public Todo createTodo(@RequestBody Todo todo) {
    return todoRepository.save(todo);
  }

  // ブラウザからのリクエストで動くのがGet
  @GetMapping("/{id}")
  public ResponseEntity<Todo> getTodoById(@PathVariable Long id) {
    Todo todo = todoRepository.findById(id).orElse(null);
    if (todo == null) {
      return ResponseEntity.notFound().build();
    }
    return ResponseEntity.ok(todo);
  }

  @PutMapping("/{id}")
  public ResponseEntity<Todo> updateTodo(@PathVariable Long id, @RequestBody Todo todoDetails) {
    Todo todo = todoRepository.findById(id).orElse(null);
    if (todo == null) {
      return ResponseEntity.notFound().build();
    }
    todo.setTitle(todoDetails.getTitle());
    todo.setStatus(todoDetails.getStatus());
    final Todo updatedTodo = todoRepository.save(todo);
    return ResponseEntity.ok(updatedTodo);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Void> deleteTodo(@PathVariable Long id) {
    Todo todo = todoRepository.findById(id).orElse(null);
    if (todo == null) {
      return ResponseEntity.notFound().build();
    }
    todoRepository.delete(todo);
    return ResponseEntity.noContent().build();
  }
}
