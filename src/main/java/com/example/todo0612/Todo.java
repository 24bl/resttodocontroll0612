package com.example.todo0612;

// import java.time.LocalDate;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class Todo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String title;
    private String status;
    private String priority;
    // private LocalDate limit;

    // Todoのデフォルトコンストラクタ
    public Todo(){
    }

    // FormクラスからEntityクラスに変化する際に使用するコンストラクタ
    public Todo(String title, String status, String priority){
        this.title = title;
        this.status = status;
        this.priority = priority;
    }

    // Getters and Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPriority() {
        return priority;
    }
      
     public void setPriority(String priority) {
        this.priority = priority;
    }

    // public LocalDate getLimit() {
    //     return limit;
    // }
      
    //  public void setLimit(LocalDate limit) {
    //     this.limit = limit;
    // }
}

