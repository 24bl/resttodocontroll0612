package com.example.todo0612;

import java.util.List;

public interface TodoService {
	
	/**
	 *  1. タスクを全件検索
	 * SELECT文で複数リスト分のTodoを返す
	 */
	List<Todo> findAll();
	
	/**
	 *  2. タスクを主キー検索	
	 * SELECT文で主キー検索を行い1人分のTodoを返す
	 */
	Todo findById(Integer id);
	
	/**
	 *  3. タスクの追加
	 * INSERT文でTodoを指定し挿入するためvoid型
	 */
	void insert(Todo todo);
	
	/**
	 *  4. 主キーで指定したタスクの更新
	 * UPDATE文でTodoを指定し更新するためvoid型
	 */
	void update(Todo todo);
	
	/**
	 *  5. 主キーで指定したタスクの削除
	 * 主キー検索を行いDELETE文で要員を削除するためvoid型
	 */
	void delete(Integer id);

	/**
	 *  6. タスクをステータス検索	
	 * SELECT文でステータス検索を行い1人分のTodoを返す
	 */
	// Todo findByStatus(String status);

}