package com.example.todo0612;

// import java.time.LocalDate;

// import org.springframework.format.annotation.DateTimeFormat;

public class TodoForm {
	
	// フィールド
	private String title;
	private String status;
	private String priority;

	// @DateTimeFormat(pattern = "MM-dd")
	// private LocalDate limit;
	
	// FormクラスをEntityクラスに変換するメソッド
  public Todo convertToEntity(){
    return new Todo(title, status, priority);
  }
  
	// getter・setter
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPriority() {
		return priority;
	}
	
	public void setPriority(String priority) {
		this.priority = priority;
	}

// 	public LocalDate getLimit() {
// 		return limit;
// }
	
//  public void setLimit(LocalDate limit) {
// 		this.limit = limit;
// }

	@Override
	public String toString() {
		return "TodoForm [title=" + title + 
				", status=" + status + 
				", priority=" + priority +"]";
	}
}