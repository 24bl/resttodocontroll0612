package com.example.todo0612;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Todo0612Application {
    public static void main(String[] args) {
        SpringApplication.run(Todo0612Application.class, args);
    }
}