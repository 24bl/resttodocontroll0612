package com.example.todo0612;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

// import ch.qos.logback.core.net.SyslogOutputStream;

// import org.springframework.web.bind.annotation.RequestBody;

@Controller

public class TodoController {

  private final TodoService todoService;

  // コンストラクタインジェクションによるDI
  public TodoController(TodoService todoService) {
    this.todoService = todoService;
  }

  @GetMapping("/")
  public String root() {
    return "redirect:index";
  }

  // 全検索 > 一覧画面に繊維
  @GetMapping("/index")
  public String index(Model model) {
    List<Todo> todoList = todoService.findAll();

    model.addAttribute("todoList", todoList);
    System.out.println("全検索");
    System.out.println("todoList : " + todoList);
    return "index";
  }

  // 一覧画面 > 詳細
  @GetMapping("/detail")
  public String findById(@RequestParam Integer id, Model model) {
    Todo todo = todoService.findById(id);
    model.addAttribute("todo", todo);
    return "detail";
  }

  // 入力画面に遷移
  @GetMapping("/regist")
  public String regist() {
    return "regist2";
  }

  // フォームに入力されたリクエストパラメータを受け取りDBへの挿入処理を行う
  // 処理後、一覧画面にリダイレクト
  @PostMapping("/insertComplete")
  public String insert(TodoForm todoForm) {
    Todo todo = todoForm.convertToEntity();
    todoService.insert(todo);
    System.out.println("===============挿入===============");
    System.out.println("タスク : " + todo);
    return "redirect:index";
  }

  @GetMapping("/edit")
  public String edit(@RequestParam Integer id, Model model) {
    Todo todo = todoService.findById(id);
    model.addAttribute("todo", todo);

    return "edit";
  }

  @PostMapping("/updateComplete")
  public String edit(@RequestParam Long id, TodoForm todoForm) {
    Todo todo = todoForm.convertToEntity();
    todo.setId(id);
    todoService.update(todo);
    System.out.println("===============更新===============");
    System.out.println("タスク : " + todo);
    return "redirect:index";
  }

  @GetMapping("/deleteComplete")
  public String edit(@RequestParam Integer id) {
    todoService.delete(id);
    System.out.println("===============削除===============");
    return "redirect:index";
  }

}